# pihole-lists
This repo contain all my pi-hole scripts.

## Windows
- flush_dns.bat : https://gitlab.com/Natizyskunk/pi-hole-scripts/raw/master/scripts/Windows/flush_dns.bat
- change_dns_as_admin.bat : https://gitlab.com/Natizyskunk/pi-hole-scripts/raw/master/scripts/Windows/change_dns_as_admin.bat
- pihole-disabler-via-api.bat : https://gitlab.com/Natizyskunk/pi-hole-scripts/raw/master/scripts/Windows/pihole-disabler-via-api.bat

## Linux
- script.sh : ...