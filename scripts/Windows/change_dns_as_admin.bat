@echo off
COLOR 0A

:start
cls
echo Please choose an option.
echo ---
echo Option 1 - List interfaces.
echo Option 2 - set new interface DNS.
echo Option 3 - Exit.
echo.
set /P option="Choice : "

:option1
IF /I %option% NEQ 1 GOTO option2
GOTO listInterfaces

:option2
IF /I %option% NEQ 2 GOTO option3
GOTO setDNS

:option3
IF /I %option% NEQ 3 GOTO restart
GOTO END

:restart
cls
echo Something went wrong. Script is restartiing...
PAUSE
GOTO START

:listInterfaces
cls
echo Actual interfaces :
echo.
netsh interface ip show config
PAUSE
GOTO start

:setDNS
cls
set /P interface="Please, enter a network interface: "
set /P confirmInterface="is the network interface '%interface%' correct ? (Y/[N])"

IF /I "%confirmInterface%" NEQ "Y" IF /I "%confirmInterface%" NEQ "y" IF /I "%confirmInterface%" NEQ "YES" IF /I "%confirmInterface%" NEQ "yes" IF /I "%confirmInterface%" NEQ "Yes" GOTO restart
GOTO DNS

:DNS
echo.
set /P DNS="Now, enter new DNS: "
set /P confirmDNS="is the new DNS '%DNS%' correct ? (Y/[N])"

IF /I "%confirmDNS%" NEQ "Y" IF /I "%confirmDNS%" NEQ "y" IF /I "%confirmDNS%" NEQ "YES" IF /I "%confirmDNS%" NEQ "yes" IF /I "%confirmDNS%" NEQ "Yes" GOTO restart
GOTO install

:install
cls
netsh interface ip set dns "%interface%" static %DNS%
echo the new dns '%DNS%' has been applied to the '%interface%' interface.
echo Please, don't forget to flush your DNS.
PAUSE
GOTO start

:END
cls
echo Script ended.
PAUSE