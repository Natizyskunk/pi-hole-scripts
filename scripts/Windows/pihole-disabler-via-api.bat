@echo off
COLOR 0A

set protocol=http
set host=<pihole_hostname_or_ip>
set APItoken=<pihole_api_token>

:START
cls
echo Please choose an option.
echo ---
echo Option 1 - Disable pihole for 'x' amount of time (in seconds).
echo Option 2 - Disable pihole for undefinded amount of time (always disable).
echo Option 3 - Enable pihole.
echo Option 4 - Exit.
echo.
set /P option="Choice : "

:option1
IF /I %option% NEQ 1 GOTO option2
GOTO disableForXSeconds

:option2
IF /I %option% NEQ 2 GOTO option3
GOTO disable

:option3
IF /I %option% NEQ 3 GOTO option4
GOTO enable

:option4
IF /I %option% NEQ 4 GOTO restart
GOTO END

:disableForXSeconds
cls
set /P timePause="Please enter the amount of time you want to disable pihole : "
echo.
echo disabling pihole for %timePause% seconds.
start "" "%protocol%://%host%/admin/api.php?disable=%timePause%&auth=%APItoken%"
TIMEOUT /T 5
GOTO start

:disable
cls
echo.
echo disabling pihole.
start "" "%protocol%://%host%/admin/api.php?disable&auth=%APItoken%"
TIMEOUT /T 5
GOTO start

:enable
cls
echo.
echo enabling pihole.
start "" "%protocol%://%host%/admin/api.php?enable&auth=%APItoken%"
TIMEOUT /T 5
GOTO start

:END
cls
echo.
echo Script ended.
TIMEOUT /T 5
exit
