@echo off
COLOR 0A

:: This script can be used after adding a domain/IP to the pi-hole whitelist
:: to be able to reach this domain/IP.
ipconfig /flushdns
ipconfig /release
ipconfig /renew
cls

echo.
echo DNS has been flushed ! :)
PAUSE